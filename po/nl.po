msgid ""
msgstr ""
"Project-Id-Version: VERSION\n"
"POT-Creation-Date: 2017-03-03 10:03+0000\n"
"PO-Revision-Date: 2017-03-03 10:03+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE TEAM <EMAIL@ADDRESS>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Address"
msgstr "Adres"

msgid "Account added!"
msgstr "Account toegevoegd!"

msgid "Amount (Ѧ)"
msgstr "Bedrag (Ѧ)"

msgid "Account deleted!"
msgstr "Account verwijderd!"

msgid "Add Delegate"
msgstr "Voeg Delegate"

msgid "Amount"
msgstr "Bedrag"

msgid "Add"
msgstr "Toevoegen"

msgid "Approval"
msgstr "Goedkeuring"

msgid "Are you sure?"
msgstr "Weet je het zeker?"

msgid "Balance"
msgstr "Saldo"

msgid "Cancel"
msgstr "Annuleer"

msgid "Exchange"
msgstr "Uitwisseling"

msgid "Fee (Ѧ)"
msgstr "Prijs "

msgid "Folder Name"
msgstr "Folder Naam"

msgid "From"
msgstr "Van"

msgid "Hand crafted with ❤ by"
msgstr "Hand gemaakt met ❤ door"

msgid "Height"
msgstr "Hoogte"

msgid "Label"
msgstr "Etiket"

msgid "Label set"
msgstr "Etiket doen"

msgid "Language"
msgstr "Taal"

msgid "Last checked"
msgstr "laatst gecontroleerd"

msgid "List full or delegate already voted."
msgstr "Lijst geheel of afgevaardigde al gestemd."

msgid "Market Cap."
msgstr "Marktkapitalisatie."

msgid "My Accounts"
msgstr "Mijn accounts"

msgid "Network connected and healthy!"
msgstr "Het netwerk aangesloten en gezond!"

msgid "Network disconected!"
msgstr "Netwerk disconected!"

msgid "No difference from original delegate list"
msgstr "Geen verschil van origineel afgevaardigde lijst"

msgid "Next"
msgstr "Volgende"

msgid "No publicKey"
msgstr "Geen publieke sleutel"

msgid "No search term"
msgstr "Geen zoekterm"

msgid "Not enough ARK on your account"
msgstr "Niet genoeg ARK op uw rekening"

msgid "Open Delegate Monitor"
msgstr "Open afgevaardigde Monitor"

msgid "Other Accounts"
msgstr "Andere accounts"

msgid "Passphrase does not match your address"
msgstr "Passphrase komt niet overeen met uw adres"

msgid "Passphrase"
msgstr "wachtwoord"

msgid "Passphrase is not corresponding to account"
msgstr "Wachtwoord is niet overeenkomt met verantwoording"

msgid "Peer"
msgstr "Turen"

msgid "Please enter a folder name."
msgstr "Geef een mapnaam."

msgid "Please enter a new address."
msgstr "Geef een nieuw adres."

msgid "Please enter a short label."
msgstr "Geef een korte Etiket."

msgid "Please enter this account passphrase to login."
msgstr "Vul deze account wachtwoord om in te loggen."

msgid "Productivity"
msgstr "Produktiviteit"

msgid "Quit"
msgstr "Ophouden"

msgid "Quit Ark Client?"
msgstr "Stoppen Ark Client?"

msgid "Rank"
msgstr "Rang"

msgid "Receive Ark"
msgstr "Ontvang Ark"

msgid "Rename"
msgstr "andere naam geven"

msgid "Remove"
msgstr "Verwijderen"

msgid "Send"
msgstr "Sturen"

msgid "Second Passphrase"
msgstr "Tweede wachtwoordzin"

msgid "Send Ark"
msgstr "Stuur Ark"

msgid "Send Ark from"
msgstr "Stuur Ark van"

msgid "you need at least 1 ARK to vote"
msgstr "U heeft minimaal 1 ARK nodig om te stemmen."

msgid "sent with success!"
msgstr "Verzonden met succes!"

msgid "Total Ѧ"
msgstr "Totaal"

msgid "This account has never interacted with the network. This is either:"
msgstr "Dit account is nog nooit samengewerkt met het netwerk. Dit is ofwel:"

msgid "an inexistant wallet, ie nobody knows the passphrase of this wallet. The ARK are lost"
msgstr "een inexistant portemonnee, dus niemand weet het wachtwoord van deze portefeuille. De ARK zijn verloren"

msgid "an inexistant wallet, ie nobody knows the passphrase of this wallet."
msgstr "Een onbekend portemonnee, dus niemand weet het wachtwoord van dit portemonnee."

msgid "a cold wallet, ie the owner of the passphrase has never used it"
msgstr "een koude portemonnee, dat wil zeggen de eigenaar van het wachtwoord heeft het nooit gebruikt"

msgid "Details"
msgstr "Details"

msgid "Recipient Address"
msgstr "ontvanger Adres"

msgid "Add Account from address"
msgstr "Account toevoegen van adres"

msgid "Add Sponsors Delegates"
msgstr "Voeg Sponsors Afgevaardigden"

msgid "By using the following forms, you are accepting their"
msgstr "Door het gebruik van de volgende vormen, gaat u akkoord met hun"

msgid "Delegate Registration"
msgstr "Delegate Registratie"

msgid "Deposit ARK"
msgstr "borg ARK"

msgid "Deposit and Withdraw through our partner"
msgstr "Storten en opnemen via onze partner"

msgid "Folders"
msgstr "Mappen"

msgid "History"
msgstr "Geschiedenis"

msgid "If you own this account, you can enable virtual folders. Virtual Folders are simple subaccounts to better organise your money, without needing to send a real transaction and pay a fee. <br>\n                        Moreover, this account will move under the \"My Accounts\" list."
msgstr "Als u eigenaar van dit account kunt u virtuele mappen in te schakelen. Virtual Folders zijn eenvoudig subaccounts om uw geld beter te organiseren, zonder dat een echte transactie te sturen en een vergoeding betalen. <br>\nBovendien zal deze account bewegen onder de \"Mijn Accounts\" lijst."

msgid "If you own this account, you can enable Voting"
msgstr "Als u eigenaar van dit account kunt u Voting inschakelen"

msgid "Maximum:"
msgstr "Maximaal:"

msgid "Minimum:"
msgstr "Minimum:"

msgid "Next..."
msgstr "Volgende..."

msgid "Please login with your account before using this feature"
msgstr "Gelieve in te loggen met uw account voordat u deze functie"

msgid "Please report any issue to"
msgstr "Meld elk probleem op"

msgid "Receive"
msgstr "Ontvangen"

msgid "Refresh"
msgstr "Verversen"

msgid "Second Signature Creation"
msgstr "Tweede Handtekening Creation"

msgid "Select a coin"
msgstr "Selecteer een muntstuk"

msgid "Second passphrase"
msgstr "Tweede wachtwoord"

msgid "Status:"
msgstr "Ttoestand:"

msgid "Status"
msgstr "Toestand"

msgid "Sponsors Page"
msgstr "Sponsors pagina"

msgid "Terms and Conditions"
msgstr "Voorwaarden"

msgid "They have funded this app so you can use it for free. <br>\n                Vote them to help the development."
msgstr "Zij hebben deze app gefinancierd, zodat je het kunt gebruiken voor gratis. <br>\nStem ze de ontwikkeling te helpen."

msgid "Transaction id"
msgstr "Transactie ID"

msgid "Valid until"
msgstr "Geldig tot"

msgid "Withdraw ARK"
msgstr "Intrekken ARK"

msgid "Your email, used to send confirmation"
msgstr "Uw e-mail, om de bevestiging te sturen"

msgid "to"
msgstr "naar"

